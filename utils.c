//
//  utils.c
//  
//
//  Created by Daniele Lubrano on 09/09/15.
//
//

#include "utils.h"

void printUChar(unsigned char ch){
    for (int i=1; i<=8; i++) {
        unsigned char bit = ch & (1 << (sizeof(char)*8 - i));
        bit = bit >> (sizeof(char)*8 - i);
        
        if (bit==0)
            printf("0");
        else
            printf("1");
        
        if (i == 4)
            printf(" ");
        
    }
    printf("\n");
}
