//
//  lib.c
//  Prog2Es
//
//  Created by Daniele Lubrano on 01/08/15.
//  Copyright (c) 2015 Daniele Lubrano. All rights reserved.
//

#include <stdio.h>
#include "lib.h"

#include <math.h>
#include <string.h>

#include "utils.h"

void funcprova(){
    printf("Prova");
}

char low_upp(char ch){
    return ch ^ (1 << 5);
}

char rotate_char(char ch, char n_bit){
    unsigned char res;
    unsigned char mask;
    if (n_bit>0){
        mask = (1 << (n_bit + 1)) - 1;
        unsigned char savedBits = ch & mask;
        savedBits = savedBits << (sizeof(char)*8 - n_bit);
        
        res = ch >> n_bit;
        res = res | savedBits;
        
    }else{
        n_bit = -n_bit;
        
        mask = (1 << (n_bit + 1)) - 1;
        mask = mask << (sizeof(char)*8 - n_bit);
        unsigned char savedBits = ch & mask;
        savedBits = savedBits >> (sizeof(char)*8 - n_bit);
        
        res = ch << n_bit;
        res = res | savedBits;
    }
    
    
    return res;
}

int val_X(short len, char c[]){
    int val = 0;
    //passing the length
    //len = sizeof(type);
    //type = char/short/long
    
    for (int off=0; off < len; off++) {
        char currentChar = c[off];
        
        for (int currBit=0; currBit < 8; currBit++) {
            // extract current bit
            char bit = currentChar & 1;
            //b(off*8 + currBit) * pow(2,off*8 + currBit)
            val += bit * pow(2, off*8 + currBit);
            //next LSB
            currentChar = currentChar >> 1;
        }
    }
    
    return val;
    
}

unsigned char extract_k_bits(unsigned char var, char k){
    unsigned char res;
    unsigned char mask = (1 << k) - 1;

    if (k<0) //MSB
        mask = mask << (8+k);
    
    res = var & mask;
    
    if (k<0)
        res = res >> (8+k);
    
    return res;
}

unsigned char base2to10(unsigned int var){
    unsigned char res = 0;
    int q,r;
    char bit = 1;
    q = var / 2;
    r = var % 2;
    //insert the bit in the right place
    res =  res | (r << (8-bit));
    while (q!=0 && bit < 8) {
        bit++;
        r = q % 2;
        q = q / 2;
        res =  res | (r << (8-bit));
    }
    //fill in the zeroes
    res = res >> (8-bit-1);

    return res;
}

unsigned char base2to10bw(unsigned int var){
    unsigned char res = 0;
    int q;
    char r;
    char bit = 1;
    
    q = var >> 1;
    r = var & 1;
    res =  res | (r << (8-bit));
    while (q!=0 && bit < 8) {
        bit++;
        r = q & 1;
        q = q >> 1;
        res =  res | (r << (8-bit));
    }
    res = res >> (8-bit-1);

    return res;
}

unsigned int base10to2(unsigned char var, char out[]){
    unsigned int res = 0;
    
    for (int i=0; i<8; i++) {
        char bit = var & 1;
        var = var >> 1;
        res += bit * pow(2, i);
    }

    out[2] = DECIMAL_DIGITS[res - (((res / 100)*100) + ((res / 10) * 10))] ;
    out[1] = DECIMAL_DIGITS[res / 10];
    out[0] = DECIMAL_DIGITS[res / 100];
    
    return res;
}

unsigned int base10to2fromCharArray(char bits[], char maxDigits){
    unsigned int res = 0;
    for (int i=0; i<maxDigits; i++) {
        char bit = bits[i] - 48;
        res += bit * pow(2, i);
    }
    
    return res;
}

unsigned char base2Sumbw(unsigned char op1, unsigned char op2){
    unsigned char res = 0;
    
    char rip = 1;
    while (rip > 0) {
        res = op1 ^ op2;
        rip = op1 & op2;
        rip = rip << 1;
        op1 = res;
        op2 = rip;
    }
    
    return res;
}
unsigned char base2Subbw(unsigned char op1,unsigned char op2){

        unsigned char sub = 0, carry=1;
        
        while (carry>0)
        {
            sub = op1^op2;
            carry = ~op1&op2;
            carry = carry<<1;
            op1 = sub;
            op2 = carry;
        }
        
        return sub;
}

unsigned char biasedNumber(unsigned char num, unsigned char N){
    unsigned char res = 0;
    if (sizeof(num)*8 < N)
        return 0;
    //evaluate bias val
    unsigned char bias = (1 << N) - 1 ; //pow(2,N) - 1
    res = num + bias;
    return res;
}

unsigned char complement2(unsigned char num, unsigned char N){
    unsigned char res = 0;
    if (sizeof(num) < N)
        return res;
    
    unsigned char power2 = 1 << N;
    res=(power2+num)%power2;
    
    return res;
}

char base2algebricSum(char op1,char op2){
    
    char res = 0;
    
    char rip = 1;
    while (rip > 0) {
        res = op1 ^ op2;
        rip = op1 & op2;
        rip = rip << 1;
        op1 = res;
        op2 = rip;
    }
    return res;
}

//Esercizi 3

typedef struct
{
    int S : 1;
    int E : 8;
    int M : 23;
    
} SEM; //32 bits

union bit32{
    float F;
    unsigned int uI;
    SEM a;
} extractor;

void extractBitsFromFloat(int* S, int* E, int* M, float f){
 
    extractor.F = f;
    int biasedExp = extractor.a.E;
    *E = biasedExp + ((1 << 7) - 1);
    *S = extractor.a.S;
    *M = extractor.a.M;
    
    printf("Float Bit Pattern\n");
    unsigned int tripOne = (1 << (sizeof(int)*8 - 1));
    for(int i = 1; i<= sizeof(SEM)*8;i++){
        if(i==2 || i==10)
            printf(" ");
        printf("%c",((tripOne&extractor.uI) >> (sizeof(int)*8 - i)) + 48);
        tripOne = tripOne >> 1;
    }
    printf("\n");
    printf("%c %4c %15c \n", 'S', 'E', 'M' );
    
}

float string2float(char *str){
    float res = 0.0F;
    float sign = (str[0]=='-')?-1.0F:1.0F;
    (sign > 0) ? printf("It's positive\n") : printf("It's negative\n");
    char * whole = strtok(str, "+-.");
    char * negative = strtok(NULL, "+-.");
    
    
    
    return res;
}
