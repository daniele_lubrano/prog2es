//
//  lib.h
//  Prog2Es
//
//  Created by Daniele Lubrano on 01/08/15.
//  Copyright (c) 2015 Daniele Lubrano. All rights reserved.
//

#ifndef Prog2Es_lib_h

static char DECIMAL_DIGITS[10] = {'0','1','2','3','4','5','6','7','8','9'};

#define Prog2Es_lib_h

#endif

 

void funcprova();
char low_upp(char);
char rotate_char(char, char);
int val_X(short, char []);
unsigned char extract_k_bits(unsigned char, char);

unsigned char base2to10(unsigned int);
unsigned char base2to10bw(unsigned int);
unsigned int base10to2(unsigned char, char[]);
unsigned int base10to2fromCharArray(char[], char);

unsigned char base2Sumbw(unsigned char, unsigned char);
unsigned char base2Subbw(unsigned char, unsigned char);

char base2algebricSum(char, char);
void extractBitsFromFloat(int*, int*, int*, float);
float string2float(char *);