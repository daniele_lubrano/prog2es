//
//  main.c
//  Prog2Es
//
//  Created by Daniele Lubrano on 01/08/15.
//  Copyright (c) 2015 Daniele Lubrano. All rights reserved.
//

#include <stdio.h>
#include "lib.h"

#include "utils.h"

#include <float.h>

union word32bit {
    long L;
    short S[2];
    char C[4];
} word;

union idontgiveafuckaboutsigns{
    unsigned char uC;
    signed char sC;
} pureBits;

typedef struct {
    unsigned char dd:5;
    unsigned char mm:4;
    unsigned char yy:4;
    
} DATE; // 2 byte circa

union justLetItBit{
    DATE d;
    unsigned char bit[2];
};

typedef struct
{
    unsigned int S : 1;
    unsigned int E : 8;
    unsigned int M : 23;
    
} SEM; //32 bits

union bit32{
    float F;
    unsigned int uI;
    SEM a;
} TEST;


int main(int argc, const char * argv[]) {
    // insert code here...
    printf("Hello, World!\n");
    
    /*

    printUChar('a');
    printUChar(rotate_char('a', -3));
    
    word.C[0] = 'a';
    
    printf("Valore decimale: %d \n",val_X(sizeof(char), word.C));

    base2to10(34);
    base2to10bw(34);
    char result[3];
    base10to2(34, result);
    for (int i=0; i<3; i++) {
        printf("%c",result[i]);
    }
    printf("\n");
    DATE currDate;
    
    currDate.dd = 18;
    currDate.mm = 4;
    currDate.yy = 06;
    
    printf("Val day:%u\n",(unsigned int)currDate.dd);
    printf("Val month:%u\n",(unsigned int)currDate.mm);
    printf("Val year:%u\n",(unsigned int)currDate.yy);

    
    printf("sizeof(oggi)= %u\n",sizeof(currDate));
    
    printf("Sizeof float = %d\n",sizeof(float));
    
    float flt = 0.0F;
    scanf("%f",&flt);
    printf("Float read = EXP %e\nStandard =  %f\n",flt,flt);
    
    int S,E,M;
    
    //extractBitsFromFloat(&S,&E,&M,flt);
    TEST.F = flt;
    unsigned int tripOne = (1 << (sizeof(int)*8 - 1)); // 1000 0000 0000 0000 0000 0000 0000 0000
    printf("\nFloat Bit Pattern\n");
    for(int i = 1; i<= sizeof(SEM)*8;i++){
        if(i==2 || i==10)
            printf(" ");
        printf("%c",((tripOne&TEST.uI) >> sizeof(int)*8 - i) + 48);
        tripOne = tripOne >> 1;
    }
    printf("\n");
    printf("%c %4c %15c \n", 'S', 'E', 'M' );
    
    
    int biasedExp = TEST.a.E;
    int fixedExp = biasedExp + ((1 << 7) - 1);
    printf("Biased EXP: %d\nFixed EXP: %d\n",biasedExp,fixedExp);
    
    
    while((getchar())!='\n');
    do {
        printf("\nPress the Enter key to continue.");
    } while (getchar() != '\n');
     */
    char numString[10];
    printf("Insert float num:\nFormat +X.Y\n");
    scanf("%s",numString);
    
    string2float(numString);
    
    
    while((getchar())!='\n');
    do {
        printf("\nPress the Enter key to continue.");
    } while (getchar() != '\n');
        
    return 0;
}
